import argparse
from appcenter import app
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="AppCenter App Server")
    parser.add_argument('-H', '--host', help="Interface to bind server to [default 127.0.0.1]", default="127.0.0.1")
    parser.add_argument("-p", "--port", default=5000, type=int, help="Port to run server on")
    args = parser.parse_args()
    app.run(host=args.host, port=args.port)