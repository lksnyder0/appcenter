# App Center
A central place to list common apps along with links to their documentation. 
## Features
* User login
* Two permission levels
  * Read-only
  * Admin
* Multiple Categories
* App Icons
* Document and App Search

## Installation
The default credentials for your first login are: 
```
admin@example.com:password
```

### Basics
* Clone repo
* Create virtual environment
* Install requirements
* Start application

### Details
```bash
git clone https://gitlab.com/lksnyder0/appcenter.git
cd appcenter
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python runserver.py -H 0.0.0.0
```

## Setup
1. Click the gree button with the plus icon to add your first app.
2. Alternativly you can go to /admin/category to add additional categories.
3. Out of the box the app has two hosted locations configured, Internal and 
   External. Feel free to add more or delete these. Each app must have one so
   leave at least one.
