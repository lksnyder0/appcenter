import os
import os.path as op

from flask import url_for
from flask_admin import AdminIndexView, form
from flask_admin.contrib.sqla import ModelView
from flask_security import current_user
from wtforms import TextAreaField, PasswordField
from jinja2 import Markup
from appcenter.database import *

file_path = op.join(op.dirname(__file__), 'static/uploads')
try:
    os.mkdir(file_path)
except OSError:
    pass

class SecureModelView(ModelView):
    def is_accessible(self):
        if current_user.is_active and current_user.has_role('admin'):
            return True
        return False

class UserView(SecureModelView):
    column_searchable_list = ['email']
    column_exclude_list = ['password']
    form_ajax_ref = {
        'roles': {
            'fields': ['name', 'description']
        }
    }
    form_overrides = {
        "password": PasswordField
    }
    
class RoleView(SecureModelView):
    column_searchable_list = ['name', 'description']

class AppView(SecureModelView):
    def _list_thumbnail(view, context, model, name):
        if not model.icon:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='uploads/' + form.thumbgen_filename(model.icon)))
    
    column_searchable_list = ['name', 'description']
    column_filters = ["category", "hosted"]
    column_formatters = {
        'icon': _list_thumbnail
    }

    form_columns = ["name", "description", "link", "icon", "category", "hosted", "documents"]    
    form_overrides = {
        'description': TextAreaField
    }
    form_ajax_ref = {
        'category': {
            'fields': ['name', 'description']
        },
        'hosted': {
            'fields': ['name', 'description']
        }
    }
    form_extra_fields = {
        'icon': form.ImageUploadField('Image',
                                      base_path=file_path,
                                      thumbnail_size=(50, 50, True),
                                      url_relative_path="uploads/")
    }

class CategoryView(SecureModelView):
    column_searchable_list = ['name', 'description']
    form_excluded_columns = ["apps"]
    
class HostedView(SecureModelView):
    column_searchable_list = ['name', 'description']
    form_excluded_columns = ["apps"]

class DocumentsView(SecureModelView):
    column_searchable_list = ["name", "description"]
    column_filters = ["app_id"]
    form_excluded_columns = ["apps"]

class SecureAdminIndexView(AdminIndexView):
    def is_accessible(self):
        if current_user.has_role("admin") and current_user.is_active:
            return True
        return False
