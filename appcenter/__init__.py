import datetime

from flask import Flask
from flask_security import Security, SQLAlchemyUserDatastore
from flask_mail import Mail
from flask_admin import Admin
from flask_admin.menu import MenuLink

from appcenter.database import *
from appcenter.adminviews import *
from appcenter.blueprints.app_center import appcenter_blueprint
from appcenter.blueprints.app_center.database import *
from appcenter.forms import SearchForm

app = Flask(__name__)
app.config.from_pyfile('appcenter.cfg')
db.init_app(app)

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

## Mail
mail = Mail(app)

## Blueprints
app.register_blueprint(appcenter_blueprint)

## Admin interfaces
admin = Admin(app, name="App Center Admin", template_mode="bootstrap3", index_view=SecureAdminIndexView())
admin.add_link(MenuLink(name="Back", category='', url="/"))
admin.add_view(AppView(App, db.session, endpoint="appsadmin"))
admin.add_view(DocumentsView(Documents, db.session))
admin.add_view(CategoryView(Category, db.session))
admin.add_view(HostedView(Hosted, db.session))
admin.add_view(UserView(User, db.session))
admin.add_view(RoleView(Role, db.session))

@app.context_processor
def templatevariables():
    categories = Category.query.order_by(Category.weight).all()
    searchform = SearchForm()
    return dict(categories=categories, searchform=searchform)

# Create first user
@app.before_first_request
def create_user():
    db.create_all()
    if len(User.query.all()) == 0:
        print("Adding default user")
        try:
            user_datastore.create_user(email='admin@example.com', password='password', confirmed_at=datetime.datetime.now())
        except IntegrityError:
            pass
        user_datastore.find_or_create_role(name="admin", description="Admin Users")
        user_datastore.add_role_to_user("admin@example.com", "admin")
        category = Category(name="Other", description="Misc apps", weight=1000)
        db.session.add(category)
        internal = Hosted(name="Internal", description="Internally hosted apps", color="#1ca001")
        external = Hosted(name="External", description="Externally hosted apps", color="#b20002")
        db.session.add(internal)
        db.session.add(external)

    db.session.commit()
