from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Category(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    description = db.Column(db.String(4096), default="")
    weight = db.Column(db.Integer(), unique=True, nullable=False)
    apps = db.relationship('App', backref='category', lazy='dynamic', cascade="all, delete-orphan")
    
    def __str__(self):
        return self.name

class Hosted(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    description = db.Column(db.String(4096), default="")
    color = db.Column(db.String(7))
    apps = db.relationship('App', backref='hosted', lazy='dynamic')
    
    def __str__(self):
        return self.name

class App(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    link = db.Column(db.String(1024), nullable=False)
    description = db.Column(db.String(4096), default="")
    icon = db.Column(db.String(1024))
    category_id = db.Column(db.Integer(), db.ForeignKey('category.id'), nullable=False)
    hosted_id = db.Column(db.Integer(), db.ForeignKey('hosted.id'), nullable=False)
    documents = db.relationship('Documents', backref='app', lazy='dynamic')
    
    def __str__(self):
        return self.name

class Documents(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    description = db.Column(db.String(4096), default="")
    link = db.Column(db.String(1024), nullable=False)
    app_id = db.Column(db.Integer(), db.ForeignKey('app.id'), nullable=False)