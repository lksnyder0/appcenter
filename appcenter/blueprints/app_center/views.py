
import json

from . import appcenter_blueprint
from .database import *
from .forms import *
from flask import render_template, redirect, flash, url_for, request, abort
from flask_security import login_required
from sqlalchemy.exc import IntegrityError

@appcenter_blueprint.route("/")
@login_required
def index():
    firstCat = Category.query.order_by(Category.weight).first()
    return redirect(url_for('.getApps', id=firstCat.id))

@appcenter_blueprint.route("/search/<terms>")
@login_required
def search(terms):
    query = "%{0}%".format(terms)
    response = {"apps":[], "docs":[]}
    response["terms"] = terms
    apps = App.query.filter( (App.name.like(query)) | (App.description.like(query)) ).all()
    docs = Documents.query.filter( (Documents.name.like(query)) | (Documents.description.like(query)) ).all()
    for each in apps:
        response["apps"].append(dict(name=each.name, description=each.description, link=each.link))
    for each in docs:
        response["docs"].append(dict(name=each.name, description=each.description, link=each.link))
    return json.dumps(response)

@appcenter_blueprint.route("/cat/<int:id>")
@login_required
def getApps(id):
    apps = App.query.filter_by(category_id=id).all()
    adddocument = AddDocumentForm()
    return render_template('apps.html', adddocument=adddocument, apps=apps)

@appcenter_blueprint.route("/documents/<int:id>")
@login_required
def getDocumentation(id):
    documents = Documents.query.filter_by(app_id=id)
    app = App.query.filter_by(id=id).first()
    return render_template('docs_list.html', id=id, catid=app.category_id, documents=documents)

@appcenter_blueprint.route("/documents/add", methods=["POST"])
@login_required
def addDocumentation():
    adddocument = AddDocumentForm()
    response = {"messages": []}
    if adddocument.validate_on_submit():
        newdocument = Documents(name=adddocument.name.data, description=adddocument.description.data, link=adddocument.link.data, app_id=adddocument.appid.data)
        try:
            db.session.add(newdocument)
            db.session.commit()
        except IntegrityError:
            response["status"] = "error"
            response["messages"].append({"name":"name", "error": "Name already exists"})
        else:
            flash("Documentation added", "success")
            response["status"] = "success"
            response["messages"].append({"action": "redirect", "location":url_for('.getApps', id=adddocument.catid.data)})
    else:
        response["status"] = "error"
        for field in adddocument:
            for error in field.errors:
                response["messages"].append({"name":field.name, "error":error})
    return json.dumps(response)