import json

from .database import *
from .forms import *
from flask import Blueprint

appcenter_blueprint = Blueprint('app_center', __name__, template_folder="templates")

from .views import *