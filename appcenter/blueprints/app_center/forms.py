from flask_wtf import Form
from wtforms import StringField, TextAreaField, HiddenField
from wtforms.validators import DataRequired, Optional, Length, URL

class AddDocumentForm(Form):
    name = StringField("Document Name", validators=[DataRequired(), Length(max=255, message="Name too long")])
    link = StringField("Link", validators=[DataRequired(), URL(message="Link must be a url")])
    description = TextAreaField("Description", validators=[Length(max=4096, message="Description too long"), Optional()])
    appid = HiddenField("app id", validators=[DataRequired()])
    catid = HiddenField("cat id", validators=[DataRequired()])

class SearchForm(Form):
    search = StringField("Search")
