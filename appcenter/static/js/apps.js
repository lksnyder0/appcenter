var submitAndVerifyForm = function (events, formData, url, csrftoken) {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
        }
    });
    $.ajax({
        url: url,
        data: formData,
        type: 'POST',
        dataType: 'json',
        encode: true
    }).done(function(data) {
        if (data["status"] == "success") {
            for (var i in data["messages"]) {
                var message = data["messages"][i];
                if (message["action"] == "redirect" ) {
                    window.location.replace(message["location"]);
                };
            };
        } else {
            for ( var i in data["messages"] ) {
                var targetField = $(events.target).find("input[name=" + data["messages"][i]["name"] +"]");
                targetField.closest(".form-group").addClass("has-danger");
                targetField.tooltip({
                    delay: {
                        "show":500,
                        "hide":500,
                    },
                    placement: 'top',
                    title: data["messages"][i]["error"],
                    trigger: 'manual'
                });
                targetField.tooltip("show");
                targetField.on('input propertychange paste', function () {
                    $(this).tooltip('dispose');
                    targetField.closest(".form-group").removeClass("has-danger");
                });
            };
        };
    });
};

var processResults = function (row, tablebody, results) {
    if (results.length > 0) {
        tablebody.empty();
        $(row).show();
        for (i in results) {
            var row = "<tr>" +
                '<td>'+results[i]["name"]+'</td>' +
                '<td>'+results[i]["description"]+'</td>' +
                '<td><a href="'+results[i]["link"]+'">Open</a></td>' +
                '</tr>'
            tablebody.append(row)
        }
    } else {
        tablebody.empty();
        $(row).hide();
    }
}

$(document).ready(function() {
    $(".alert-message").alert();
	window.setTimeout(function() { $(".alert-message").alert('close'); }, 4000);
    $("#navbar").find("a").each(function () {
        if ( $(this).attr('href') == window.location.pathname) {
            $(this).addClass("active");
        }
    });

	$('#search').keypress(function (e) {
		if (e.which == 13) {
			var searchbox = $(this);
			var searchterms = searchbox.val()
			if (searchterms.length > 2) {
				results = $.getJSON("/search/" + escape(searchterms), function ( data ) {
                    if ( (data["apps"].length > 0) || (data["docs"].length > 0) ) {
                        $("#noresultsmsg").hide();
                    } else {
                        $("#noresultsmsg").show();
                    };

                    var appstable = $("#appsresultstable").find("tbody");
                    var appsrow = $("#appsresultrow");
                    if (data["apps"].length > 0) {
                        appstable.empty();
                        appsrow.show();
                        for (i in data["apps"]) {
                            var row = "<tr>" +
                                '<td>'+data["apps"][i]["name"]+'</td>' +
                                '<td>'+data["apps"][i]["description"]+'</td>' +
                                '<td><a href="'+data["apps"][i]["link"]+'" target="_blank">Open</a></td>' +
                                '</tr>';
                            appstable.append(row);
                        }
                    } else {
                        appstable.empty();
                        appsrow.hide();
                    };

                    var docstablebody = $("#docsresultstable").find("tbody");
                    var docsrow = $("#docsresultrow");
                    if (data["docs"].length > 0) {
                        docstablebody.empty();
                        docsrow.show();
                        for (i in data["docs"]) {
                            var row = "<tr>" +
                                '<td>'+data["docs"][i]["name"]+'</td>' +
                                '<td>'+data["docs"][i]["description"]+'</td>' +
                                '<td><a href="'+data["docs"][i]["link"]+'" target="_blank">Open</a></td>' +
                                '</tr>';
                            docstablebody.append(row);
                        }
                    } else {
                        docstablebody.empty();
                        docsrow.hide();
                    };
				});
				$('#searchresults').modal('show');
			}
			else if (searchterms.length <= 2) {
				console.log("need terms");
				searchbox.closest(".form-group").addClass("has-danger");
				searchbox.tooltip({
					delay: {
						"show": 500,
						"hide": 500
					},
					placement:"bottom",
					title: "Search too short",
					trigger: "manual"
				});
				searchbox.tooltip("show");
				searchbox.on('input propertychange paste', function() {
					searchbox.tooltip('dispose');
					searchbox.closest(".form-group").removeClass("has-danger");
				});
			};
			return false;
		};
	});

    $('#documentlist').on("show.bs.modal", function (event) {
        var appid = $(event.relatedTarget).data('appid');
        var modal = $(this);
        modal.find(".modal-content").empty();
        $.ajax('/documents/' + appid).done(function(html) {
            modal.find(".modal-content").append(html);
        });
        modal.find("#adddocuments").data("appid", appid);
    });
    $("#adddocument").on("show.bs.modal", function (event) {
        $("#documentlist").modal("hide");
        var appid = $(event.relatedTarget).data('appid');
        var catid = $(event.relatedTarget).data('catid');
        var modal = $(this);
        modal.find("#appid").val(appid);
        modal.find("#catid").val(catid);
    });
    $('#adddocument').on("hide.bs.modal", function (event) {
        var modal = $(this);
        modal.find('#adddocumentform')[0].reset();
        modal.find('.has-danger').each(function () {
            $(this).removeClass('has-danger')
        })
        modal.find('input').each(function () {
            $(this).tooltip('dispose');
        });
    });
    $('#adddocumentform').on("submit", function(event) {
        var csrftoken = $(event.target).find("input[name=csrf_token]").val();
        $(event.target).find("textarea").each(function () {
            escape($(this).val());
        });
        var formData = $(event.target).serialize();
        submitAndVerifyForm(event, formData, '/documents/add', csrftoken);
        event.preventDefault();
        return false;
    })
});